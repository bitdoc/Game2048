package com.evan.game2048;

import java.util.ArrayList;
import java.util.List;

import android.R.integer;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.provider.Telephony.Mms.Addr;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridLayout;

public class GameView extends GridLayout {

    private CardView[][] cardViews = new CardView[4][4];
    private List<Point> emptyPoints = new ArrayList<Point>();
    
	public GameView(Context context) {
		super(context);
		initGameView();
	}

	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initGameView();
	}
	
	public GameView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initGameView();
	}
	
	private void initGameView() {
		
		setBackgroundColor(Color.GRAY);
		setColumnCount(4);
		
		setOnTouchListener(new View.OnTouchListener() {
			
			private float startX = -1,startY = -1,offsetX,offsetY;
			final private float min_distance = 5;
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					startX = event.getX();
					startY = event.getY();
					break;
				case MotionEvent.ACTION_UP:
					offsetX = event.getX() - startX;
					offsetY = event.getY() - startY;
					if(Math.abs(offsetX) > Math.abs(offsetY) && Math.abs(offsetX) > min_distance ) {
						if(offsetX < 0) {
							swipeLeft();
						} else {
							swipeRight();
						}
					}
					else if(Math.abs(offsetX) <= Math.abs(offsetY) && Math.abs(offsetY) > min_distance ) {
						if(offsetY < 0) {
							swipeUp();
						} else {
							swipeDown();
						}
					}
					startX = -1;
					startY = -1;
					break;
				default:
					break;
				}
				return true;
			}
		});
	}
	
	private void swipeLeft() {
		for(int y=0;y<4;y++) {
			for(int x=0;x<4;x++) {
				for(int x1=x+1;x1<4;x1++) {
					if(cardViews[x1][y].getNum() > 0) {
						if(cardViews[x][y].getNum() <= 0) {
							cardViews[x][y].setNum(cardViews[x1][y].getNum());
						    cardViews[x1][y].setNum(0);
						} 
						else if(cardViews[x][y].equals(cardViews[x1][y])){
							cardViews[x][y].setNum(2*cardViews[x][y].getNum());
							cardViews[x1][y].setNum(0);
						}
						
					}
				}
			}
		}
		addRandomNum();
	}
	
	private void swipeRight() {
		for(int y=0;y<4;y++) {
			for(int x=3;x>=0;x--) {
				for(int x1=x-1;x1>=0;x1--) {
					if(cardViews[x1][y].getNum() > 0) {
						if(cardViews[x][y].getNum() <= 0) {
							cardViews[x][y].setNum(cardViews[x1][y].getNum());
						    cardViews[x1][y].setNum(0);
						} 
						else if(cardViews[x][y].equals(cardViews[x1][y])){
							cardViews[x][y].setNum(2*cardViews[x][y].getNum());
							cardViews[x1][y].setNum(0);
						}
						
					}
				}
			}
		}
		addRandomNum();
	}
	
	private void swipeUp() {
		for(int x=0;x<4;x++) {
			for(int y=0;y<4;y++) {
				for(int y1=y+1;y1<4;y1++) {
					if(cardViews[x][y1].getNum() > 0) {
						if(cardViews[x][y].getNum() <= 0) {
							cardViews[x][y].setNum(cardViews[x][y1].getNum());
						    cardViews[x][y1].setNum(0);
						} 
						else if(cardViews[x][y].equals(cardViews[x][y1])){
							cardViews[x][y].setNum(2*cardViews[x][y].getNum());
							cardViews[x][y1].setNum(0);
						}
					}
				}
			}
		}
		addRandomNum();
	}
	
	private void swipeDown() {
		for(int x=0;x<4;x++) {
			for(int y=3;y>=0;y--) {
				for(int y1=y-1;y1>=0;y1--) {
					if(cardViews[x][y1].getNum() > 0) {
						if(cardViews[x][y].getNum() <= 0) {
							cardViews[x][y].setNum(cardViews[x][y1].getNum());
						    cardViews[x][y1].setNum(0);
						} 
						else if(cardViews[x][y].equals(cardViews[x][y1])){
							cardViews[x][y].setNum(2*cardViews[x][y].getNum());
							cardViews[x][y1].setNum(0);
						}
					}
				}
			}
		}
		addRandomNum();
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		int cardW = (Math.min(w, h)-10)/4;
		addCards(cardW,cardW);
		startGame();
	}
	
	private void addCards(int w,int h) {
		CardView card;
		for (int y = 0; y < 4; y++) {
			for(int x = 0;x < 4;x ++) {
				card = new CardView(getContext());
				card.setNum(0);
				addView(card,w,h);
				cardViews[x][y] = card;
			}
		}
	}
	
	private void addRandomNum() {
		emptyPoints.clear();
		for (int y = 0; y < 4; y++) {
			for(int x = 0;x < 4;x ++) {
				if(cardViews[x][y].getNum() <= 0) {
					emptyPoints.add(new Point(x,y));
				}
			}
		}
		Point p = emptyPoints.remove((int)(Math.random()*emptyPoints.size()));
		cardViews[p.x][p.y].setNum(Math.random()>0.1?2:4);
	}
	
	private void startGame() {
		for (int y = 0; y < 4; y++) {
			for(int x = 0;x < 4;x ++) {
				cardViews[x][y].setNum(0);
			}	
		}
		addRandomNum();
		addRandomNum();
	}
	
	
}
